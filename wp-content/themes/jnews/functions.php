<?php
/**
 * @author : Jegtheme
 */

defined( 'JNEWS_THEME_URL' )        or define( 'JNEWS_THEME_URL', get_parent_theme_file_uri() );
defined( 'JNEWS_THEME_FILE' )       or define( 'JNEWS_THEME_FILE', __FILE__ );
defined( 'JNEWS_THEME_DIR' )        or define( 'JNEWS_THEME_DIR', plugin_dir_path( __FILE__ ) );
defined( 'JNEWS_THEME_NAMESPACE' )  or define( 'JNEWS_THEME_NAMESPACE', 'JNews_' );
defined( 'JNEWS_THEME_CLASSPATH' )  or define( 'JNEWS_THEME_CLASSPATH', JNEWS_THEME_DIR . 'class/' );
defined( 'JNEWS_THEME_CLASS' )      or define( 'JNEWS_THEME_CLASS', 'class/' );
defined( 'JNEWS_THEME_ID' )         or define( 'JNEWS_THEME_ID', 20566392 );
defined( 'JNEWS_THEME_TEXTDOMAIN' ) or define( 'JNEWS_THEME_TEXTDOMAIN', 'jnews' );

// TGM
if ( is_admin() ) {
	require get_parent_theme_file_path( 'tgm/plugin-list.php' );
}

// Theme Class
require get_parent_theme_file_path( 'class/autoload.php' );

JNews\Init::getInstance();
use Facebook\InstantArticles\Elements\Ad;

function get_post_primary_category( $post = 0, $taxonomy = 'category' ){
    if ( ! $post ) {
        $post = get_the_ID();
    }

    $terms        = get_the_terms( $post, $taxonomy );
    $primary_term = array();

    if ( $terms ) {
        $term_display = '';
        $term_slug    = '';
        $term_link    = '';
        if ( class_exists( 'WPSEO_Primary_Term' ) ) {
            $wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term               = get_term( $wpseo_primary_term );
            if ( is_wp_error( $term ) ) {
                $term_display = $terms[0]->name;
                $term_slug    = $terms[0]->slug;
                $term_link    = get_term_link( $terms[0]->term_id );
            } else {
                $term_display = $term->name;
                $term_slug    = $term->slug;
                $term_link    = get_term_link( $term->term_id );
            }
        } else {
            $term_display = $terms[0]->name;
            $term_slug    = $terms[0]->slug;
            $term_link    = get_term_link( $terms[0]->term_id );
        }
        $primary_term['url']   = $term_link;
        $primary_term['slug']  = $term_slug;
        $primary_term['title'] = $term_display;
    }
    return $primary_term;
}
add_shortcode('videoDLM', function($atts){
    $html = '';
    $idDLM = $atts['id'];
    $typeDLM = $atts['type'];
    if(!empty($idDLM)){
        $html .= '<div class="video_wrapper_responsive vd-destacado"><div id="'.$typeDLM.'-DLM" data-id="'.$idDLM.'"></div></div>';
    wp_enqueue_script('DMapi', 'https://api.dmcdn.net/all.js');
    }  
    return $html;
});

/*add_filter('instant_articles_transformed_element', function ($article) {
    $url = site_url();
    $settings_ads = Instant_Articles_Option_Ads::get_option_decoded();
    $code = new DOMDocument();
    $fragment = $code->createDocumentFragment();
    $valid_html = @$fragment->appendXML( $settings_ads['embed_code'] );
    $ad = Ad::create()
        ->withWidth(300)
        ->withHeight(250)
        ->enableDefaultForReuse();
    $article->getHeader()
        ->addAd(
            $ad->withHTML(
                $fragment
            )
        );
    return $article;
});*/

add_shortcode('playlistHome',function(){
    $html = '';
    $urlp = 'https://api.dailymotion.com/videos?fields=title%2Cid%2Cthumbnail_medium_url&owners=x19f1b1&tags=recomendado&limit=4';
    $html .= '<div class="dm_video_list">';
    $dataDM = file_get_contents($urlp);
    $playlist = json_decode($dataDM);
    foreach ($playlist->list as $index => $video) {
        $videoDMp = $video->id;
        $titleDMp = $video->title;
        $imageDMp = $video->thumbnail_medium_url;
        if($index == 0){
            $html .= '<div class="dm_video_content">
                <div class="dm_video_holder">
                    <div class="dm_video_container">
                    <iframe src="https://www.dailymotion.com/embed/video/'.$videoDMp.'?ads_params=category%253Dhome%2526site%3Destrending&queue-autoplay-next=false&queue-enable=false&sharing-enable=false" width="100%" height="100%" frameborder="0" allowfullscreen="true"></iframe>
                    </div>
                </div>
            </div>';
            $html .= '<div class="dm_side_content">';
            $html .= '<div class="dm_item_side currentdm" data-id="'.$videoDMp .'">
                <div class="dm_thumb_video">
                    <img src="https://www.estrending.com/wp-content/themes/jnews/assets/img/jeg-empty.png" data-src="'.$imageDMp.'" class="wp-post-image lazyload" />
                </div>
                <div class="dm_title_video">
                    <h3>'.$titleDMp.'</h3>
                </div>
            </div>';
        }else{
            $html .= '<div class="dm_item_side" data-id="'.$videoDMp .'">
                <div class="dm_thumb_video">
                    <img src="https://www.estrending.com/wp-content/themes/jnews/assets/img/jeg-empty.png" data-src="'.$imageDMp.'" class="wp-post-image lazyload" />
                </div>
                <div class="dm_title_video">
                    <h3>'.$titleDMp.'</h3>
                </div>
            </div>';
        }
    }
    $html .= '</div>';
    $html .= '</div>';
    return $html;
});

add_filter('instant_articles_content','custom_ad_content',10,1);



function videos_content( $content ) {
    if ( is_single() && 'post' == get_post_type() ) {

        $custom_content = "";

        $custom_content .= $content;
        $live = false;
        $video_DLM = get_post_meta( get_the_ID(), 'video_recomendado', true );
        
        $post_tags = get_the_tags( get_the_ID());
        if( has_tag( 'branded' ) ) {
            }else{
            if(!empty($video_DLM)){
                $custom_content .= '<strong>Te puede interesar: </strong><br />';
                $custom_content .= do_shortcode('[videoDLM id="'.$video_DLM.'" type="recomendado" title="'.$tt_recomendado.'"]');
                }
            }
        return $custom_content;
    } else {
        return $content;
    }
}
add_filter( 'the_content', 'videos_content' );

function custom_ad_content($content){
    $id = get_the_id();
    $vdjsd = get_post_meta( $id, 'video_destacado', true );
    $vdjsr = get_post_meta( $id, 'video_recomendado', true );
    $recipe = get_post_meta($id,'enable_food_recipe',true);

    if(!empty($vdjsd)){
        $videjod = '<iframe frameborder="0" width="300" height="300" src="https://www.dailymotion.com/embed/video/'.$vdjsd.'?ads_params=site%3Destrending_rs&sharing-enable=false&queue-enable=false&queue-autoplay-next=false&quality=380&autoplay=true"></iframe>';
        $content = $videjod.$content;
    }
    if(!empty($vdjsr)){
        $videjor = '<iframe frameborder="0" width="300" height="300" src="https://www.dailymotion.com/embed/video/'.$vdjsr.'?ads_params=site%3Destrending_rs&sharing-enable=false&queue-enable=false&queue-autoplay-next=false&quality=380&autoplay=true"></iframe>';
        $content = $content.$videjor;
    }
    if($recipe==1){
        include ABSPATH .'wp-content/plugins/jnews-food-recipe/class.jnews-food-recipe.php';
        $temp = JNews_Food_Recipe::getInstance();
        $content = $temp->render_food_recipe_content($content);
        //$content = $id;
    }
    return $content;
}
add_action( 'init', function(){
    wp_embed_register_handler( 
        'myig', 
        '/https?\:\/\/(?:www.)?instagram.com\/p\/(.+)/',   // <-- Adjust this to your needs!
        'ig_embed_handler' 
    );
} );
//clean ig embed
function ig_embed_handler( $matches, $attr, $url, $rawattr ){
    $idIG = $matches[1];
    if(!empty($idIG)){
        if(strpos($idIG, 'photo') !== false){
            $idIG = str_replace('photo/', '', $idIG);
            $baseurl = 'https://graph.facebook.com/v10.0/instagram_oembed?url=instagram.com/p/'.$idIG.'&fields=thumbnail_url,author_name&access_token=757874017927860|9eae7061d9a8d7177a64e84ff2a45bb2';
            $dataDIG = file_get_contents($baseurl);
            $igEmbed = json_decode($dataDIG,true);
            $imgIG = $igEmbed["thumbnail_url"];
            $authorIG = $igEmbed["author_name"];
            if(!empty($imgIG)){
                $html = '<figure style="width: 100%" class="wp-caption"><a href="https://www.instagram.com/p/'.$idIG.'"><img src="'.$imgIG.'" alt=""></a><figcaption class="wp-caption-text"><a href="https://www.instagram.com/'.$authorIG.'", target="_blank" rel="noopener">Fuente: instagram/'.$authorIG.' </a></figcaption></figure>';
                return $html;
            }
        }else{
            $baseurl = 'https://graph.facebook.com/v10.0/instagram_oembed?url=instagram.com/p/'.$idIG.'&fields=html&access_token=446337026596225|6ecffb63a90e3fa01434225706abd2fd';
            $dataDIG = file_get_contents($baseurl);
            $igEmbed = json_decode($dataDIG,true);
            $embedIG = $igEmbed["html"];
            if(!empty($embedIG)){
                $newhtml = strip_tags($embedIG, '<blockquote><div><figure><p><a><script>');
                $newhtml = '<div class="embed">'.$newhtml.'</div>';
                return $newhtml;
            }
        }
    }
    
}

function custom_embed_settings($code){
    if(strpos($code, 'dai.ly') !== false || strpos($code, 'dailymotion.com') !== false){
        $return = preg_replace("@src=(['\"])?([^'\">\s]*)@", "src=$1$2?queue-autoplay-next=false&queue-enable=false&sharing-enable=false", $code);
        return '<div class="video_wrapper_responsive embed">'.$return.'</div>';
    }
    if(strpos($code, 'tiktok.com') !== false){
        return '<div class="embed">'.$code.'</div>';
    }
    return $code;

}

add_filter('embed_handler_html', 'custom_embed_settings');
add_filter('embed_oembed_html', 'custom_embed_settings');

add_action('wp_head', 'customSetTargetting',1);

function customSetTargetting(){
    $tags = '<script>';
    if(is_single()) {
        $type_post = 'news';
        $tempID = get_the_ID();
        $prm_ct = get_post_primary_category($tempID, 'category'); 
        $dmVideo = get_post_meta($tempID, 'video_destacado', true );
        $post_author_id = get_post_field( 'post_author', $tempID );
        $autor_email = get_the_author_meta('nicename',$post_author_id);
        if(!empty($autor_email)){
            $tags .= 'var autorET = "'.$autor_email.'";';
        }
        if(!empty($prm_ct)){
            $tags .= 'var primaryCat = "'.$prm_ct['slug'].'";';
        }
        $categoriesp = get_the_category();
        if(!empty($categoriesp)){
            $varcats = '';
            foreach( $categoriesp as $ctp ) {
                $varcats .=  "'".$ctp->name."',";
            }
            $tags .= 'var post_categories = ['.$varcats.'];';
        }
        if(!empty($dmVideo) or has_tag('video')){
            $type_post = "video";   
        }elseif(has_tag('radio')){
            $type_post = "radio";   
        }elseif(has_tag('podcast')){
            $type_post = "podcast";   
        }
        $tags .= 'var tt_content = "'.$type_post.'";'; 
        $vertical = 'entretenimiento';
        $tags .= 'var tt_tag = "'.$vertical.'";';
    }
    if(is_category()){
        $pc = 'var post_categories = ["categoria"];';
        $cate = get_the_category();
        $cat_name = $cate[0]->slug;
        if(!empty($cat_name)){
            $pc = 'var post_categories = ["'.$cat_name.'"];';
        }
        $tags .= 'var tt_content = "page";'.$pc.' var tt_tag = "home";'; 
    }
    if (is_page() && !is_front_page()){
        $slug = 'page';
        $slug = rtrim(get_permalink(), '/');
        $slug = basename($slug);
        $tags .= 'var tt_content = "page"; var primaryCat = "page"; var tt_tag ="'.$slug.'";'; 
    } 
    if (is_front_page()){
        $tags .= 'var tt_content = "home"; var post_categories = ["home"]; var tt_tag = "home";'; 
    } 
    $tags .= '</script>';
    echo $tags;
}
function media_add_author_dropdown()
{
    $scr = get_current_screen();
    if ( $scr->base !== 'upload' ) return;

    $author   = filter_input(INPUT_GET, 'author', FILTER_SANITIZE_STRING );
    $selected = (int)$author > 0 ? $author : '-1';
    $args = array(
        'show_option_none'   => 'All Authors',
        'name'               => 'author',
        'selected'           => $selected
    );
    wp_dropdown_users( $args );
}
add_action('restrict_manage_posts', 'media_add_author_dropdown');
function label_column($cols) {
    $cols["label"] = "Label";
    return $cols;
}

function label_value($column_name, $id) {
    echo  get_the_content($id);
}

function label_column_sortable($cols) {
    $cols["label"] = "name";
    return $cols;
} 
function hook_new_media_columns() {
    add_filter('manage_media_columns', 'label_column');
    add_action('manage_media_custom_column', 'label_value', 10, 2);
    add_filter('manage_upload_sortable_columns', 'label_column_sortable');
}

add_action('admin_init', 'hook_new_media_columns');

function remove_unused_lib() {
    if (!is_admin()) {
        wp_dequeue_style('js_composer_front');
        wp_dequeue_script('comment-reply');
        wp_deregister_style('js_composer_front');
     }
    if(!is_page('contacto')){
        wp_dequeue_style('contact-form-7');
        wp_dequeue_script('contact-form-7');
    }
    if(!is_single()){
        wp_dequeue_style('jnews-previewslider');
        wp_dequeue_style('jnews-previewslider-responsive');
        wp_dequeue_script('jnews-autoload');
        wp_deregister_style('jnews-previewslider');
        wp_deregister_style('jnews-previewslider-responsive');
        wp_dequeue_style('jnews-food-recipe-css');
    }
    wp_dequeue_style('wp-mediaelement');
    wp_dequeue_script('wp-mediaelement');
    wp_deregister_script('wp-mediaelement');
    wp_deregister_style('wp-mediaelement');
}

add_action( 'wp_enqueue_scripts', 'remove_unused_lib' );

//Allow disable Facebook Instant Article
add_filter("instant_articles_should_submit_post", "ad_validation_FBIA",10, 2);
function ad_validation_FBIA($should_show, $IA_object){
    $validateIA = get_post_meta($IA_object->get_the_id(), "disable_ia");
    if($validateIA){
        return false;
    }else{
        return true;
    }   
}
add_action('post_submitbox_misc_actions', 'create_disable_ia');
add_action('save_post', 'save_disable_ia');
add_action('post_submitbox_misc_actions', 'create_liveblog');
add_action('save_post', 'save_liveblog');
function create_disable_ia(){
    $post_id = get_the_ID();
  
    if (get_post_type($post_id) != 'post') {
        return;
    }
  
    $value = get_post_meta($post_id, 'disable_ia', true);
    wp_nonce_field('ad_disable_ia_nonce_'.$post_id, 'ad_disable_ia_nonce');
    ?>
    <div class="misc-pub-section misc-pub-section-last">
        <label><input type="checkbox" value="1" <?php checked($value, true, true); ?> name="disable_ia" /><?php _e('Disable this post in FB-IA', 'pmg'); ?></label>
    </div>
    <?php
}
function save_disable_ia($post_id){
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    
    if (
        !isset($_POST['ad_disable_ia_nonce']) ||
        !wp_verify_nonce($_POST['ad_disable_ia_nonce'], 'ad_disable_ia_nonce_'.$post_id)
    ) {
        return;
    }
    
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }
    if (isset($_POST['disable_ia'])) {
        update_post_meta($post_id, 'disable_ia', $_POST['disable_ia']);
    } else {
        delete_post_meta($post_id, 'disable_ia');
    }
}
function user_can_richedit_custom() {
    global $wp_rich_edit;
    if (get_user_option('rich_editing') == 'true' || !is_user_logged_in()) {
        $wp_rich_edit = true;
        return true;
    }
    $wp_rich_edit = false;
    return false;
}


add_action('wp_head', 'adOPs_calls',10);
function adOPs_calls(){
    $adTagCall = '';
    $typep = '';
    $varcontrol = 1;
    if(is_single()){
        if(has_tag('noads') or has_tag('branded')){
            $varcontrol  = 0;
        }
        $tempID = get_the_ID();
        $prm_ct = get_post_primary_category($tempID, 'category');
        if(!empty($prm_ct)){
            $temp_section = $prm_ct['slug'];
            $section = $temp_section;

        }
        $typep = 'nodes';
    }
    if(is_page()){
        if(is_page( array( 47,3,340))){
            $varcontrol = 0;
        }else{
            $varcontrol = 1;
            $typep = 'page';
        }
        
    }
    if($varcontrol == 1){
        $tags = '<!--'.$varcontrol.'--><script>';
        $tags .= '(function($) {
        var sizesScroll = [[728, 90],[970, 90]];
        var sizesTop = [[728, 90],[970, 90]];
        var sizesMid = [[728, 90],[728,250]];
        var sizesSqr1 = [300,250];
        var sizesSqr6 = [[728, 90],[970, 90]];
        var sizesFoot = [[728, 90],[970, 90],[1,1]];
        if ($(window).width() < 720) {
            sizesScroll = [320,50];
            sizesTop = [[320,100],[320,50]];
            sizesMid = [[320,50],[300,250],[300,600]];
            sizesSqr1 = [[320, 100],[320, 50],[300, 250],[300, 600]];
            sizesSqr6 = [[320, 100],[320, 50],[300, 600]];
            sizesFoot = [[320, 50],[1,1]];
        }
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() { '; 
        $tags .= 'googletag.defineSlot("/22167442/scrollbanner_mid", sizesScroll, "scrollbanner").addService(googletag.pubads());
        googletag.defineSlot("/22167442/top_banner_atf", sizesTop, "topbanner").addService(googletag.pubads());
        googletag.defineSlot("/22167442/midbanner_mid", sizesMid, "midbanner").addService(googletag.pubads());
        googletag.defineSlot("/22167442/sqrbanner1_atf", sizesSqr1, "sqrbanner1").addService(googletag.pubads());
        googletag.defineSlot("/22167442/sqrbanner6_mid", sizesSqr6, "sqrbanner6").addService(googletag.pubads());
        googletag.defineSlot("/22167442/footbanner_btf", sizesFoot, "footbanner").setTargeting("refresh","true").addService(googletag.pubads());
        googletag.defineSlot("/22167442/native_overlayer",[1,1], "overlay_1x1").addService(googletag.pubads());';
        if(is_single()){
            $tags .= 'googletag.defineSlot("/22167442/native_intext",[1,1], "intext_1x1").addService(googletag.pubads());';
        }
        if(is_front_page()){
            $typep = 'home';    
        }
        if(is_category()){
            $catSite = get_category( get_query_var( 'cat' ), false );
            if ( ! empty( $catSite ) ) {
                $temp_section  = $catSite->slug; 
                $section = $temp_section;
            }
            $typep = 'category';    
        }
        $tags .= 'googletag.pubads().setTargeting("site", "yusi");';
        if(is_front_page()){
            $tags .= 'googletag.pubads().setTargeting("tipo_de_pag", "home");';
        }else{
            $tags .= 'googletag.pubads().setTargeting("tipo_de_pag", "'.$typep.'");';
            $tags .= 'googletag.pubads().setTargeting("category", "'.$section.'");';
        }
        $tags .= 'googletag.pubads().collapseEmptyDivs();';
        $tags .= 'var SECONDS_TO_WAIT_AFTER_VIEWABILITY = 30;
        googletag.pubads().addEventListener("impressionViewable", function(event) {
            var slot = event.slot;
            if (slot.getTargeting("refresh").indexOf("true") > -1) {
            setTimeout(function() {
                googletag.pubads().refresh([slot]);
            }, SECONDS_TO_WAIT_AFTER_VIEWABILITY * 1000);
            }
        });
        googletag.pubads().addEventListener("slotRenderEnded", function(event) {
            if (event.slot.getSlotElementId() == "footbanner") {
                $("#wrap_footer_banner").fadeIn("fast");  
            }
        });';
        $tags .= 'googletag.enableServices(); }); })(jQuery);';
        $tags .= '</script>';
        echo $tags;
    }
}
function add_meta_robots_tags($robots) {
    if(is_single()){
        $post_year = get_the_date( 'Y' );
        $post_month = get_the_date( 'm' );
        if($post_month<8){
            return 'noindex, nofollow';
        }
        if(has_tag('exclude')){
            return 'noindex, nofollow';
        }
    }
    return $robots;
}
add_filter("wpseo_robots", 'add_meta_robots_tags');